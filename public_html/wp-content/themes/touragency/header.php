<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php wp_head(); ?>
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <h1><a href="<?php echo home_url(''); ?>"> <?php bloginfo('name'); ?></a> <small><?php bloginfo('description'); ?></small></h1>
            </div>
            <?php wp_nav_menu(['theme_location' => 'primary', 'menu_class' => 'nav nav-pills']);  ?>
        </div>
        