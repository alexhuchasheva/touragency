<?php get_header(); ?>
<div class="container">
    <p>This is a specific template for post slug tretij-blok-tsitat </p>
   <?php while(have_posts()): the_post() ?>

    <h2><?php the_title(); ?></h2>
    
    <?php the_content(); ?>
    
    <?php endwhile; ?>
</div>
<?php get_footer(); ?>
