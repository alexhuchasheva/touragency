<?php get_header(); ?>
<div class="container">

   <?php while(have_posts()): the_post() ?>

    <div class="row">
        <div class="col-sm-9">
            <h2><a href="<?php the_permalink(); ?>">
                <?php the_field('country'); ?>/
                <?php the_field('city'); ?>/
                <?php the_title(); ?>/
                <?php the_field('days'); ?> д./
                <?php the_field('nights'); ?> н./
                <?php the_field('stars'); ?>*/
                </a></h2>
            <?php the_excerpt(); ?>
        </div>
        <div class="col-sm-3"><?php the_post_thumbnail('thumbnail'); ?></div>
    </div>
    
    <?php endwhile; ?>
</div>
<?php get_footer(); ?>
